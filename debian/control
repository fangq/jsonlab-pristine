Source: octave-jsonlab
Maintainer: Qianqian Fang <fangqq@gmail.com>
Section: science
Priority: optional
Standards-Version: 4.5.0
Build-Depends: debhelper-compat (= 12), dh-octave
Homepage: https://openjdata.org/jsonlab
Vcs-Git: https://salsa.debian.org/pkg-octave-team/octave-jsonlab.git
Vcs-Browser: https://salsa.debian.org/pkg-octave-team/octave-jsonlab
Rules-Requires-Root: no


Package: octave-jsonlab
Architecture: all
Depends: octave-zmat, ${octave:Depends}, ${misc:Depends}
Description: native JSON/UBJSON/MassagePack encoder/decoder for Octave
 JSONLab is a free and open-source JSON/UBJSON/MessagePack encoder and
 decoder written in the native MATLAB language. It can be used to convert a
 MATLAB data structure (array, struct, cell, struct array, cell array, and
 objects) into JSON/UBJSON/MessagePack formatted strings and files, or to
 parse a JSON/UBJSON/MessagePack file into MATLAB data structure. JSONLab
 supports both MATLAB and GNU Octave.
 .
 JSONLab is not just a parser and writer of JSON/UBJSON data files, but one
 that systematically converts complex scientific data structures into
 human-readable and universally supported JSON forms using the standardized
 JData data annotations.
 .
 This package provides support for Octave.


Package: matlab-jsonlab
Section: contrib/science
Architecture: all
Depends: octave-zmat, matlab-support, ${misc:Depends}
Description: native JSON/UBJSON/MassagePack encoder/decoder for MATLAB
 JSONLab is a free and open-source JSON/UBJSON/MessagePack encoder and
 decoder written in the native MATLAB language. It can be used to convert a
 MATLAB data structure (array, struct, cell, struct array, cell array, and
 objects) into JSON/UBJSON/MessagePack formatted strings and files, or to
 parse a JSON/UBJSON/MessagePack file into MATLAB data structure. JSONLab
 supports both MATLAB and GNU Octave.
 .
 JSONLab is not just a parser and writer of JSON/UBJSON data files, but one
 that systematically converts complex scientific data structures into
 human-readable and universally supported JSON forms using the standardized
 JData data annotations.
 .
 This package provides support for MATLAB.
